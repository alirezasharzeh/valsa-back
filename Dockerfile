FROM node:16.6.2

RUN mkdir -p /valsa
WORKDIR /valsa

EXPOSE 3000

ENTRYPOINT ["./entrypoint.sh"]

